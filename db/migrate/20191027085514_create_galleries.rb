class CreateGalleries < ActiveRecord::Migration[6.0]
  def change
    create_table :galleries do |t|
      t.string :title, null: false
      t.boolean :is_private, default: false

      t.timestamps
    end
  end
end
