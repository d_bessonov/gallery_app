class RemoveTypeFromResources < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :resource_type, :boolean
  end
end
