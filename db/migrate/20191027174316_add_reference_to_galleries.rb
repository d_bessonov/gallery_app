class AddReferenceToGalleries < ActiveRecord::Migration[6.0]
  def change
    add_reference :galleries, :user, foreign_key: true
  end
end
