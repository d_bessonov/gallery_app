class CreateResources < ActiveRecord::Migration[6.0]
  def change
    create_table :resources do |t|
      t.string :name, null: false
      t.boolean :type, null: false
      t.text :description
      t.references :gallery, null: false, foreign_key: true

      t.timestamps
    end
  end
end
