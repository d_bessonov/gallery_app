Rails.application.routes.draw do
  devise_for :users
  get 'galleries/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users

  resources :galleries do
      resources :resources
  end

  match '/users',   to: 'users#index',   via: 'get'
  match '/users/:id',     to: 'users#show',       via: 'get'
  # get '/galleries/:gallery_id/resources/:id',     to: 'resources#next',      as: 'next_gallery_resource'

  root 'galleries#index'
end
