class GalleriesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @galleries = Gallery.all
  end

  def show
    @gallery = Gallery.find(params[:id])
  end

  def new
    @gallery = Gallery.new
  end

  def edit
    @gallery = Gallery.find(params[:id])
  end

  def create
    @gallery = current_user.galleries.create(gallery_params)
    # @gallery = Gallery.new(gallery_params)

    if @gallery.save
      redirect_to @gallery
    else
      render 'new'
    end
  end

  def update
    @gallery = Gallery.find(params[:id])

    if @gallery.update(gallery_params)
      redirect_to @gallery
    else
      render 'edit'
    end
  end

  def destroy
    @gallery = Gallery.find(params[:id])
    @gallery.destroy

    redirect_to galleries_path
  end

  private
    def gallery_params
      params.require(:gallery).permit(:title, :is_private)
    end
end
