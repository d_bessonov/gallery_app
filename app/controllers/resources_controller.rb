class ResourcesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :has_rights, except: :show

  def index
    @resources = Resource.all
  end

  def show
    # @resource = Resource.find(params[:id])

    @gallery = Gallery.find(params[:gallery_id])
    @resource = @gallery.resources.find(params[:id])
    # resources = @gallery.resources.order('resources.id ASC')
    # @resource = resources.find(params[:id])

    @next = @gallery.resources.where('resources.id > ?', @resource.id).first
    @prev = @gallery.resources.where('resources.id < ?', @resource.id).last
  end

  def new
    @gallery = Gallery.find(params[:gallery_id])
    @resource = @gallery.resources.new
  end

  def edit
    @gallery = Gallery.find(params[:gallery_id])
    @resource = Resource.find(params[:id])
  end

  def create
    @gallery = Gallery.find(params[:gallery_id])
    @resource = @gallery.resources.create(resource_params)

    if @resource.save
      redirect_to gallery_path(@gallery)
    else
      render 'new'
    end
  end

  def update
    @gallery = Gallery.find(params[:gallery_id])
    @resource = @gallery.resources.find(params[:id])

    if @resource.update(resource_params)
      redirect_to [@gallery, @resource]
    else
      render 'edit'
    end
  end

  def destroy
    @gallery = Gallery.find(params[:gallery_id])

    @resource = @gallery.resources.find(params[:id])
    @resource.file.purge
    @resource.destroy
    redirect_to gallery_path(@gallery)
  end

  private
    def resource_params
      params.require(:resource).permit(:name, :description, :file)
    end

    def has_rights
      @gallery = Gallery.find(params[:gallery_id])

      # @resource = @gallery.resources.find(params[:id])
      # @resource = Resource.find(params[:id])
      if current_user != @gallery.user
          redirect_to galleries_path
      end
    end
end
