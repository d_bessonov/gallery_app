class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :has_rights, except: :show

  def index
      @users = User.all
  end

  def show
      @user = User.find_by_id(params[:id])
  end

  def update
      @user = User.find(params[:id])
      if @user.update_attributes(secure_params)
          redirect_to users_path, :notice => "User updated."
      else
          redirect_to users_path, :alert => "Unable to update user."
      end
  end

  private
      def has_rights
          if current_user.user?
              redirect_to galleries_path
          end
      end

      def secure_params
          params.require(:user).permit(:role)
      end
end
