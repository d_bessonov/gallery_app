class Resource < ApplicationRecord
    belongs_to :gallery
    has_one_attached :file
    validates :name, presence: true, length: { minimum: 5 }
end
