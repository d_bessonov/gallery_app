class Gallery < ApplicationRecord
    belongs_to :user
    has_many :resources, dependent: :destroy
    validates :title, presence: true, length: { minimum: 5 }
end
